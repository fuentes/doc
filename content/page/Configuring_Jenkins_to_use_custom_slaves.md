---
title: "Configuring Jenkins to use custom slaves"
---
TODO

### Providing custom build slaves using the CloudStack portal

Slaves instances are obtained through
[CloudStack](http://cloudstack.org/); refer to the CloudStack
documentation and to the [CloudStack
tutorial](../qualif_cloudstack_tutorial "wikilink"). In the following
sections, we assume that you have created a build slave, know its IP
address and can SSH into it (using a newly-created *ci* account, for
instance).

**Note (2012-08-03):** new instances come by default with a *root*
account (password: *password*).

**Note (2012-11-06):** new windows instances come by default with an
administrator account named *ci* (password: *ci*).

**Note:** in this section, we will set up a CentOS Linux slave. Other
systems may require adjustments.

To add a *ci* user on a new Linux machine:

`   # useradd ci`

![](/doc/img/CI-Jenkins-WebPortal-SSHKey.png "/doc/img/CI-Jenkins-WebPortal-SSHKey.png")

You need to enable Jenkins access to that user. So get the Jenkins SSH
public key using the
![](/doc/img/CI-Jenkins-WebPortal-SSHKey-Button.png "fig:/doc/img/CI-Jenkins-WebPortal-SSHKey-Button.png")
button from the Slave tab of your project on the Web Portal and place
the key in *\~ci/.ssh/authorized\_keys*.

Slaves nodes should at least provide a Java Runtime Environment.

Note: prefer an Oracle JRE when choosing a Java environment for the
build slaves. The Oracle JRE can be installed after a manual download
(because of a license agreement).

*OpenJDK* is very close to the Oracle version (most of the code is
shared) and can be installed under CentOS Linux directly from the fedora
package manager:

`   # yum install java-1.8.0-openjdk`

This package only contains the JRE. The JDK is in
*java-1.8.0-openjdk-devel*.

To add a build slave, browse the *Manage Jenkins/Manage Nodes* section,
then choose *New node*.

In the dialog, give the node a name and select "dumb slave".

![](/doc/img/CI-Jenkins-Host-Basic-Config.png "/doc/img/CI-Jenkins-Host-Basic-Config.png")

Now configure the host settings to allow ssh connection (in the
*Advanced* section of the *host* item)

![](/doc/img/CI-Jenkins-Host-Advanced-Config.png "/doc/img/CI-Jenkins-Host-Advanced-Config.png")

Uses *ci* for username and
*/net/www/ci/**<projectshortname>**/.ssh/id\_rsa* for private key file.

Now save and you are done.

### Providing Windows build slaves

#### Windows Slaves Plugin

The "Windows Slaves Plugin" is required. You can check if it is
installed on page
<https://ci.inria.fr/><your project name>/pluginManager/installed

#### Configuring the Jenkins node

To provide a windows build slave, you have to declare it under Jenkins
by going to *Manage Jenkins/Manage Nodes/New node*' (if the VM was not
created by the Web portal):

![](/doc/img/Jenkins-WindowsSlaveConfiguration.png "/doc/img/Jenkins-WindowsSlaveConfiguration.png")

Please select the launch method “Let Jenkins control this Windows
slave as a Windows service” (if this option does not appear, please
install the “Windows Slaves Plugin”). Put “.\ci” as administrator user
name, and the associated password (“ci” is the default password). Put
the *IP address* of the slave as host name (and does *not* put the
host name, as Jenkins would refuse to connect with the error “A
duplicate name exists on the network.”). For the field “Run service
as”, choose “Use Administrator account given above”.

#### Installing the Jenkins agent

Now log into your virtual machine with a **ci** account (see [ the
CloudStack
tutorial](../qualif_cloudstack_tutorial#windows-users "wikilink") how to
log on a windows VM).

To install a Jenkins agent for windows you need to install the Oracle
JRE from the web (you need to accept the license agreement).

After that, using a web browser go to your Jenkins project in the
**Manage Jenkins/Manage Node** and select the previously created node,
you should get the following screen:

![](/doc/img/Jenkins-WindowsSlaveNode.png "/doc/img/Jenkins-WindowsSlaveNode.png")

Click on the **Launch** button and follow the procedure to finish the
slave configuration. It will output an "Hudson slave agent" window
saying that the slave agent is running. You should install this slave
agent as a windows service by selecting the *File \> Install as a
Windows Service* and answer yes to all questions:

![](/doc/img/Jenkins-WindowsSlaveInstall.png "/doc/img/Jenkins-WindowsSlaveInstall.png")
