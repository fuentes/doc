These instructions explain how to recreate the `windows-10` template
with OpenSSH server and Chocolatey.

# Create a Windows 10 slave

This page supposes that a slave has been created in a project in
https://qlf-ci.inria.fr from one of the two templates `Win10-CI-2-GTE`
or `windows-10` (`Community` tab).

`windows-10` template is derived from `Win10-CI-2-GTE`
with an Open SSH server and Chocolatey installed: these steps are
documented below.

Choose `Medium instance-W10: 2x2 GHz + 4 GB RAM`
for “Computer offering”.

# Install OpenSSH Server

The setup procedure of OpenSSH Server on Windows 10 is documented on
Microsoft's website.
https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse

According to the page linked above, the procedure should work on
Windows Server 2019 and from Windows 10 1809.

The installation can be performed with Powershell launched as
administrator (search for Powershell in start menu, right-click on it,
and choose “Run as Administrator”)

You may run the following command to check that `OpenSSH.Server`
capability is available and not installed in your system (note
that you should be able to copy/paste from your machine to the
Powershell console displayed by `rdesktop`: just press `Ctrl+V`
to paste in the console).

```
Get-WindowsCapability -Online | ? Name -like 'OpenSSH*'
```

You should get the following output.

```
Name  : OpenSSH.Client~~~~0.0.1.0
State : Installed

Name  : OpenSSH.Server~~~~0.0.1.0
State : NotPresent
```

The following command installs the `OpenSSH.Server`
capability (if a different version number is displayed in the
output of the command above, you should copy the displayed
version number accordingly).

```
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
```

In the case of the output displays `RestartNeeded : True`, then
the slave should be restarted. You would then have to run
Powershell as administrator again.

To finalize the installation and start the `sshd` service,
execute the following commands in Powershell.

```
Start-Service sshd
Set-Service -Name sshd -StartupType 'Automatic'
```

The initial default shell when opening an SSH connection is
`cmd.exe`. This default can be changed by setting the
string value `DefaultShell` in the registry key
`HKEY_LOCAL_MACHINE\SOFTWARE\OpenSSH`.
For example, the following Powershell command line sets
Powershell as the default shell.
For more details about configuring OpenSSH, see
https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_server_configuration

Warning: Jenkins expects `cmd.exe` command line on Windows
environment.  Making Powershell the default shell will prevent
Jenkins from using the agent.

```
New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -PropertyType String -Force
```

The following command restores `cmd.exe` as the
default shell.

```
New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\Windows\System32\cmd.exe" -PropertyType String -Force
```

Note that you may always run the commands `powershell` or `cmd` to
enter into a Powershell or a Windows command line.

# Install Chocolatey

The installation instruction are given on Chocolatey website.
https://chocolatey.org/install

The following command has to be executed
in Windows command line (`cmd.exe`) as administrator
and installs Chocolatey.

```
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
```

Surprisingly, `choco` command seems to be available from `ssh`
sessions only after restart (`Restart-Service sshd` is not enough).

