#!/bin/sh
set -ex
# The script iterates each time on all branches to compile the pages.
# The artifact needs to contain all the pages and I don't see any
# mean to combine all the artifacts produced by all the branches 
# (There is the "needs:" field in .gitlab.yml to invoke an artifact
# produced by an other branch, but this would require to hard-wire all
# the branch names in .gitlab.yml...)

# $branches get all the branch names.
# Remotes refs are of the form "refs/remotes/origin/branch-name"
# so we strip the three first path components to keep only "branch-name".
# We don't keep neither "dont_publish.*" branches nor HEAD branch.
branches="`\
  git branch --remote --format='%(refname:lstrip=3)' |\
  grep -v '^dont_publish\|^HEAD$'`"
# The branches/ directory will temporarily contain one directory with
# the generated pages for each branch
mkdir branches
for branch in $branches; do
  # We are in detached HEAD: use "git reset" to force getting the branch
  git reset --hard origin/$branch
  # If branch is not master, we use toml-cli (a tool written in rust that
  # is installed with cargo) to patch config.toml to prefix the base URL
  # with the path to the branch to have correct internal links.
  if [ $branch != "master" ]; then
    ~ci/.cargo/bin/toml set config.toml baseURL \
      "https://inria-ci.gitlabpages.inria.fr/doc/branches/$branch/" \
      >config_new.toml
    mv config_new.toml config.toml
  fi
  hugo || true
  mv public branches/$branch
done
# The master branch becomes the public root, and other branches go in
# the branches/ subdirectory of public.
mv branches/master public
mv branches public/
